Lambda Sheets
---
![alt text](https://gitlab.com/jacksarick/lambda-sheets/raw/master/images/logo_small.png "I made this")

My attempt at making a light-weight spreadsheet editor with Electron.

*Logo inspired by [logodust](http://logodust.com/) No. 28*