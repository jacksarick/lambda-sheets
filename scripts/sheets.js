var tools = require('./scripts/tools.js');
var cells = {};

// Generate default sheet
function gen_start(HEIGHT, WIDTH) {
	// We increment HEIGHT by 1, and start at 1, so the table starts at 1
	HEIGHT++;
	for (var n = 1; n < HEIGHT; n++) {
		cells[n] = {};
		for (var m = 0; m < WIDTH; m++) {
			cells[n][m] = "Test";
		}
	}
}

// Turn the 2D array of cells into an HTML table
function render_sheet(){
	var template, columnLetters;
	columnLetters = ["<td style='border: 0px solid;'></td>"];

	// Clear #main
	$("#main").empty();

	// Loop through cell rows
	for (var row in cells) {
		
		// Add corresponding row
		template = "<tr id='row" + row + "'></tr>";
		$("#main").append(template);

		// Add row title
		template = "<td class='cell' id='row_title" + row + "'><b>" + row + "</b></td>";
		$("#row" + row).append(template);

		// Loop through columns
		for (var column in cells[row]) {
			// Add column, with it's cells data
			template = "<td class='cell' id='column" + tools.get_alpha(column) + "' contenteditable='true'>" + evaluate_cell(cells[row][column]) + "</td>";
			// Push the title to an array that we can prepend to the top of the sheet
			$("#row"+row).append(template);
		}
	}

	// Make column titles
	for (var column in cells[1]) {
		// Add column title
		template = "<td class='cell' id='column_title" + tools.get_alpha(column) + "'><b>" + tools.get_alpha(column) + "</b></td>";
		columnLetters.push(template);
	}

	// Prepend our column titles to the sheet
	template = "<tr id='column_titles'>" + columnLetters.join("") + "</tr>";
	$("#main").prepend(template);
}

// Get the contents of a cell, with all features
function get_cell_content(cell) {
	var contents = get_raw_cell_content(cell);

	// If the first and last char is "=", evaluate the cell
	if (contents[0] == "=") {
		evaluate_cell(contents);
	}

	else {
		return contents;
	}
}

// Get the raw contents of a cell
function get_raw_cell_content(cell) {
	//Regex means "Split into digits and non-digits"
	cell = cell.match(/(\d+|[^\d]+)/g);
	// we find the cell by finding the matching child column in the row
	var searchCell = "#row" + cell[1] + " > #column" + cell[0];
	var contents = $(searchCell).text();
	return contents;
}

// Evaluate the contents of a cell
//// Pass in the contents of a cell, not cell coords
function evaluate_cell(cell_content) {
	//TODO: Make custom rules and such

	// This is a redundant check
	if (cell_content[0] == "=") {
		// Split into list
		cell_content = cell_content.split("");

		// remove first element
		cell_content.shift();

		// turn back into string
		cell_content = cell_content.join("");
		
		// evaluate
		return eval(cell_content);
	}

	else {
		return cell_content;
	}
}