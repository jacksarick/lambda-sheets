// Function to get the base-26 conversion of a decimal number
exports.get_alpha = function() {
	if (arguments.length == 1) {
		var result, remainder, digit, num;

		// Get the argument, add 1 because of the zero index
		num = arguments[0];
		num ++;

		result = "";
		// While there is something left of the number...
		while (num > 0) {
			num--; // 1 => a, not 0 => a

			// It's some weird math, that is kind of hard to explain, but I think it's relatively easy to figure out by yourself
			////There are 26 letter in the alphabet
			remainder = num % 26;
			digit = String.fromCharCode(remainder + 65); //65 is A in ASCII
			result = digit + result;
			num = (num - remainder) / 26;
		}

		return result;
	}

	else {
		return undefined;
	}
}