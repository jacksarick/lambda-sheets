Honestly, I'd rather you not contribute (yet).

Formatting guidelines
---
- Comment often
- Lines shouldn't be *too* long (about column 120 w/o indents)
- Indent with tabs
- Open a brace on the same line as the logic ( `if (...){` )
- DO NOT PUT THE CLOSE BRACE ON THE SAME LINE AS AN ELSE (not this -> `} else {` ) [This annoys me slightly more than misaligned floor tiles, and slightly less than incomplete puzzles]
- Variables should be statedLikeThis functions_look_like_this
	- Try to keep variables a single descriptive word, functions should be a couple words